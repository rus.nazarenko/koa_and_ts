import * as Koa from 'koa'
const app = new Koa()
import userRouter from './router/user'
import authRouter from './router/auth'
import * as bodyParser from 'koa-bodyparser'

const APP_PORT = process.env.PORT

app.use(bodyParser())
app.use(authRouter.routes())
app.use(userRouter.routes())

app.listen(APP_PORT, () => {
  console.log('server is listening on port ', APP_PORT)
})
