import { Context } from 'koa';
import { userArray, User, Session, sessionArray } from '../db'


export const loginHandle = (ctx: Context) => {

  const user = ctx.request.body as unknown as User
  const existedUser = userArray.find((item) => item.name === user.name)

  if (existedUser && existedUser.password === user.password) {

    const newSession: Session = {
      userId: existedUser.id,
      counter: 0,
      expires: new Date()
    }

    const existedSession = sessionArray.find((item) => item.userId === existedUser.id)

    if (!existedSession)sessionArray.push(newSession)

    console.log("sessionArray: ", sessionArray)

    ctx.cookies.set(
      'session_key',
      Buffer.from(JSON.stringify(newSession)).toString('base64'),
      { httpOnly: true }
    )
    ctx.body = { registered: true }
  } else {
    ctx.cookies.set('session_key', '', { httpOnly: true })
    ctx.body = { registered: false }
    ctx.status = 404
  }
}