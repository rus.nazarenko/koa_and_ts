import { Context, Next } from 'koa'
import { userArray, sessionArray } from '../db'

export const cookieHandle = (ctx: Context, next: Next) => {

  if (!ctx.cookies.get('session_key')) {
    ctx.body = "User is not authorized"
    ctx.status = 401
    return
  }

  const cookiesStr:string = Buffer.from(ctx.cookies.get('session_key'), 'base64').toString('ascii')
  const dataSession = JSON.parse(cookiesStr)
  const currentUser = sessionArray.find((item) => item.userId === dataSession.userId)

  if (!currentUser) {
    ctx.body = "User is not authorized"
    ctx.status = 401
    return
  }

  const currentDate: number = new Date().getTime()
  const startDate: number = new Date(currentUser.expires).getTime()
  const diffDate: number = currentDate - startDate
  const diffMinutes: number = Math.floor(diffDate / 60000)

  if (diffMinutes > 5) {
    ctx.body = "Session timed out"
    ctx.status = 401
    return
  }

  currentUser.counter++
  console.log("sessionArray: ", sessionArray)
  next()
}

export const getUserList = (ctx: Context) => {
  ctx.status = 200
  ctx.body = userArray
}


export const getUser = (ctx: Context) => {
  const data = userArray.filter((item) => item.id === Number(ctx.params.id))
  ctx.status = 200
  ctx.body = data
}
