export interface User {
  name: string;
  id: number;
  password: string;
}

export const userArray: Array<User> = [
  {id: 1, name: 'Dima', password: '111'},
  {id: 2, name: 'Sasha', password: '222'},
  {id: 3, name: 'Maxim', password: '333'}
]

export interface Session {
  userId: number,
  counter: number,
  expires: any
}


export const sessionArray: Array<Session> = []