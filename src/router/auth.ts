import * as Router  from 'koa-router';
import { loginHandle } from '../controller/auth'

const router = new Router({ prefix: '/login' })

router.post('/', loginHandle);

export default router;
