import * as Router  from 'koa-router'
import {getUserList, cookieHandle, getUser} from '../controller/user'

const router = new Router({ prefix: '/user' });

router.get('/', cookieHandle, getUserList)
router.get('/:id', cookieHandle, getUser)

export default router;
